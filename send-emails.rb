#!/usr/bin/env ruby
# encoding: UTF-8
# coding: UTF-8

require 'mail'

$to   = 'presidencia.cndh@cndh.org.mx'
$from = 'no-reply@salvemosinternet.tk'

# Sends email
def send_email user

  # Choose random text option
  # Write opts in string as @[opt1,opt2,opt3]
  def random_text string
    matches  = string.scan(/@\[[^@]+\]/)

    matches.each do |m|
      opts = m.gsub(/@\[(.+)\]/, '\1').split(/\s*,\s*/)
      opt  = opts[rand(0..opts.length - 1)]
      string.gsub!(m, opt)
    end

    return string
  end

  # Randomizes subject
  def random_subject
    subjects = File.read('src/subjects.txt').split(/\n+/)
    subject  = subjects[rand(0..subjects.length - 1)]

    return random_text(subject)
  end

  # Randomizes body
  def random_body user
    return random_text(File.read('src/body.html')
                           .gsub('@user', user))
  end

  # Configures delivery
  Mail.defaults do
    delivery_method :smtp,
      :address    => 'smtp.dreamhost.com',
      :port       => 587,
      :domain     => 'salvemosinternet.tk',
      :user_name  => 'no-reply@salvemosinternet.tk',
      :password   => '',
      :authentication       => 'login',
      :enable_starttls_auto => false
  end

  # Sends email
  Mail.deliver do
    to      $to
    from    $from
    subject 'Soy ' + user + ': ' + random_subject
    html_part do
      content_type  'text/html; charset=UTF-8'
      body          random_body(user)
    end
  end
end

# Allows user control
Dir.chdir(__dir__)
users_new = File.read('src/users_new.txt').split(/\n+/)
users_old = File.read('src/users_old.txt').split(/\n+/)
users = []
users_hold = []
i = 0

# Sends emails
users_new.each do |user|
  if i < 99
    if !users_old.include? user
      puts 'Sending => ' + user
      send_email(user) 
      users.push(user)
      i += 1
    end
  else
    users_hold.push(user)
  end
end

# Sets new users as old
users_old = users_old.concat(users)
users_old.sort!
users_old.uniq!

# Saves data
file = File.open('src/users_old.txt', 'w:utf-8')
file.puts users_old
file.close
file = File.open('src/users_new.txt', 'w:utf-8')
file.puts users_hold
file.close
